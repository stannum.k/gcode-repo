;On layer change script for PrusaSlicer, changes retract length by 0.2mm every 10mm of height
;M207/M208 for FWRETRACT, M117 classic slicer software retracts
;retract tower step # - {int(layer_z) / 10}
;tower step height - 10mm
;retract length start - 0.1mm
;retract length inc - 0.2mm
;set reract/unretract 30mm/s, length, z-hop height
M207 F1800 S{0.1 + int(layer_z) / 10 * 0.2} Z0
M208 F1800 S{0.1 + int(layer_z) / 10 * 0.2} Z0
M117 L#{layer_num + 1}/[total_layer_count], R{0.1 + int(layer_z) / 10 * 0.2};

